define([
    "app"
], function (app) {

    var header = {
        type: "header",
        template: app.config.name
    };

    var menu = {
        view: "tree",
        id: "top:menu",
        template: "{common.folder()}<span>#value#</span>",
        width: 250,
        type: {
            folder: function (obj) {
                if (obj.$count && obj.open) {
                    return "<span class='webix_icon fa-arrow-circle-o-down'></span>";
                } else if (obj.$count && !obj.open) {
                    return "<span class='webix_icon fa-arrow-circle-o-up'></span>";
                }
                else {
                    return "<span class='webix_icon'></span>";
                }

            }
        },
        data: [
            {
                id: "root", value: "Films data", open: true, data: [
                {
                    id: "1", open: true, value: "The Shawshank Redemption", data: [
                    {id: "1.1", value: "Part 1"},
                    {
                        id: "1.2", value: "Part 2", data: [
                        {id: "1.2.1", value: "Page 1"},
                        {id: "1.2.2", value: "Page 2"},
                        {id: "1.2.3", value: "Page 3"},
                        {id: "1.2.4", value: "Page 4"},
                        {id: "1.2.5", value: "Page 5"}
                    ]
                    },
                    {id: "1.3", value: "Part 3"}
                ]
                },
                {
                    id: "2", open: true, value: "The Godfather", data: [
                    {id: "2.1", value: "Part 1"},
                    {id: "2.2", value: "Part 2"},
                    {id: "2.3", value: "Part 1"},
                    {id: "2.4", value: "Part 2"},
                    {id: "2.5", value: "Part 1"},
                    {id: "2.6", value: "Part 2"},
                    {id: "2.7", value: "Part 1"},
                    {id: "2.8", value: "Part 2"},
                    {id: "2.9", value: "Part 1"},
                    {id: "2.10", value: "Part 2"},
                    {id: "2.11", value: "Part 1"},
                    {id: "2.12", value: "Part 2"},
                    {id: "2.13", value: "Part 1"},
                    {id: "2.14", value: "Part 2"},
                    {id: "2.15", value: "Part 1"},
                    {id: "2.16", value: "Part 2"},
                    {id: "2.17", value: "Part 1"},
                    {id: "2.23", value: "Part 2"},
                    {id: "2.24", value: "Part 1"},
                    {id: "2.18", value: "Part 2"},
                    {id: "2.19", value: "Part 1"},
                    {id: "2.20", value: "Part 2"},
                    {id: "2.21", value: "Part 1"},
                    {id: "2.22", value: "Part 2"}
                ]
                }
            ]
            }
        ],
        on: {
            onItemClick: function (id, e, node) {
                var item = this.getItem(id),
                    isBranch = this.isBranch(id),
                    isOpen = this.isBranchOpen(id);
                if (isBranch && isOpen) {
                    this.close(id);
                } else if (isBranch && !isOpen) {
                    this.open(id);
                } else {
                    webix.message('RRR');
                }

            },
        }
    };

    var topToolbar = {
        view: 'toolbar',
        id:"top:toolbar",
        cols:[
            { view:"button", id:"LoadBut", value:"Load", width:100, align:"left" },
            { view:"button", value:"Save", width:100, align:"center" },
            { view:"button", value:"Info", width:100, align:"right" }
        ]
    };
    var ui = {
            type: "line",
            rows: [
                topToolbar,
                {
                    cols: [
                        {
                            type: "clean",
                            css: "app-left-panel",
                            padding: 10,
                            margin: 20,
                            borderless: true,
                            rows: [
                                header,
                                menu
                            ]
                        },
                        {
                            rows: [
                                {
                                    height: 10
                                },
                                {
                                    type: "clean",
                                    css: "app-right-panel",
                                    padding: 4,
                                    rows: [
                                        {
                                            $subview: true
                                        }
                                    ]
                                }
                            ]
                        }
                    ]

                }
            ]

        }
        ;

    return {
        $ui: ui,
        $menu: "top:menu"
    };
});
